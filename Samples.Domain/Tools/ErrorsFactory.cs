﻿using System;
using Samples.Domain.Models.Errors;

namespace Samples.Domain.Tools
{
    //TODO: remove this class
    public sealed class ErrorsFactory2
    {
        public static DomainErrorDto CreateError(Exception exception, string userMessage, bool showSystemErrors = false)
        {
            if (!showSystemErrors) return ExceptionToError(exception, userMessage);

            var error = ExceptionToError(exception, userMessage, true);

            var innerEx = exception.InnerException;
            var innerErrorContainer = error;

            //while (innerEx != null)
            //{
            //    var innerError = ExceptionToError(exception, userMessage, true);
            //    innerErrorContainer.InnerError = innerError;

            //    innerErrorContainer = innerErrorContainer.InnerError;
            //    innerEx = innerEx.InnerException;
            //}

            return error;
        }

        public static DomainErrorDto CreateError(string userMessage)
        {
            return new DomainErrorDto
            {
                UserMessage = userMessage,
                //SystemMessage = userMessage
            };
        }

        public static DomainErrorDto CreateError(string userMessage, string systemMessage)
        {
            return new DomainErrorDto
            {
                UserMessage = userMessage,
                //SystemMessage = systemMessage
            };
        }

        private static DomainErrorDto ExceptionToError(Exception exception, string userMessage, bool showSystemErrors = false)
        {
            var error = new DomainErrorDto
            {
                UserMessage = userMessage
            };

            if (exception == null) return error;
            if (!showSystemErrors) return error;

            //error.SystemMessage = exception.Message;
            //error.StackTrace = exception.StackTrace;

            return error;
        }
    }
}