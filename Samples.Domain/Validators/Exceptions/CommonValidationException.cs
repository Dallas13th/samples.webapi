﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Samples.Domain.Validators.Exceptions
{
    public sealed class CommonValidationException : Exception
    {
        public CommonValidationException(IEnumerable<ValidationResult> validationResults)
        {
            ValidationResults = validationResults;
        }

        public IEnumerable<ValidationResult> ValidationResults { get; }
    }
}