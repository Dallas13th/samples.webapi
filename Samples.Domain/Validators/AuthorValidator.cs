﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Samples.DataAccess.Contexts;
using Samples.Domain.Models.Dto;
using Samples.Domain.Resources.Locale;

namespace Samples.Domain.Validators
{
    public sealed class AuthorValidator : ValidatorBase
    {
        private readonly ICommonResource _resource;
        private readonly SamplesContext _context;

        public AuthorValidator(ICommonResource resource, SamplesContext context)
        {
            _resource = resource;
            _context = context;
        }

        public async Task ValidateCreationAsync(SimpleDto dto)
        {
            DtoIsNotNull(dto);

            EnsureIsValid();

            NameIsCorrect(dto);

            var isExist = await _context.Author.AnyAsync(w => w.FullName == dto.Name);

            if (isExist)
            {
                AddError(_resource.AuthorAlreadyExists, nameof(SimpleDto.Name));
            }
            
            EnsureIsValid();
        }

        public async Task ValidateUpdatingAsync(SimpleDto dto)
        {
            DtoIsNotNull(dto);

            EnsureIsValid();

            NameIsCorrect(dto);

            var isExist = await _context.Author.AnyAsync(w => w.FullName == dto.Name);

            if (!isExist)
            {
                AddError(_resource.AuthorNotFound, nameof(SimpleDto.Id));
            }

            EnsureIsValid();
        }

        public async Task ValidateDeletingAsync(int id)
        {
            var isExist = await _context.Author.AnyAsync(w => w.Id == id);

            if (!isExist)
            {
                AddError(_resource.AuthorNotFound);
            }

            EnsureIsValid();
        }
        
        private void DtoIsNotNull(SimpleDto dto)
        {
            if (dto == null)
            {
                AddError(_resource.AuthorNotPassed);
            }
        }

        private void NameIsCorrect(SimpleDto dto)
        {
            if (string.IsNullOrWhiteSpace(dto.Name))
            {
                AddError(_resource.AuthorIncorrectName, nameof(SimpleDto.Name));
            }
        }
    }
}