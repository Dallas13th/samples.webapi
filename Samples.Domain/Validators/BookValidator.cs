﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Samples.DataAccess.Contexts;
using Samples.Domain.Models.Dto;
using Samples.Domain.Resources.Locale;

namespace Samples.Domain.Validators
{
    public sealed class BookValidator : ValidatorBase
    {
        private readonly ICommonResource _resource;
        private readonly SamplesContext _context;

        public BookValidator(ICommonResource resource, SamplesContext context)
        {
            _resource = resource;
            _context = context;
        }

        public async Task ValidateCreationAsync(BookDto dto)
        {
            DtoIsNotNull(dto);

            EnsureIsValid();

            NameIsCorrect(dto);
            
            await AuthorIsExistAsync(dto);

            await GenreIsExistAsync(dto);

            var isExist = await _context.Book
                .AnyAsync(w => w.Name == dto.Name
                               && w.AuthorId == dto.AuthorId
                               && w.GenreId == dto.GenreId);

            if (isExist)
            {
                var props = new List<string>
                {
                    nameof(BookDto.Name),
                    nameof(BookDto.AuthorName),
                    nameof(BookDto.GenreName),
                    nameof(BookDto.AuthorId),
                    nameof(BookDto.GenreId)
                };

                AddError(_resource.BookAlreadyExists, props);
            }
            
            EnsureIsValid();
        }

        public async Task ValidateUpdatingAsync(BookDto dto)
        {
            DtoIsNotNull(dto);

            EnsureIsValid();

            NameIsCorrect(dto);

            await AuthorIsExistAsync(dto);

            await GenreIsExistAsync(dto);

            var isExist = await _context.Book.AnyAsync(w => w.Id == dto.Id);

            if (!isExist)
            {
                AddError(_resource.BookNotFound);
            }

            EnsureIsValid();
        }

        public async Task ValidateDeletingAsync(int id)
        {
            var isExist = await _context.Book.AnyAsync(w => w.Id == id);

            if (!isExist)
            {
                AddError(_resource.BookNotFound);
            }

            EnsureIsValid();
        }

        private void DtoIsNotNull(BookDto dto)
        {
            if (dto == null)
            {
                AddError(_resource.BookNotPassed);
            }
        }

        private void NameIsCorrect(BookDto dto)
        {
            if (string.IsNullOrWhiteSpace(dto.Name))
            {
                AddError(_resource.BookIncorrectName, nameof(BookDto.Name));
            }
        }

        private async Task AuthorIsExistAsync(BookDto dto)
        {
            var isExist = await _context.Author.AnyAsync(w => w.Id == dto.AuthorId);

            if (!isExist)
            {
                var props = new List<string>
                {
                    nameof(BookDto.AuthorName),
                    nameof(BookDto.AuthorId)
                };

                AddError(_resource.AuthorNotFound, props);
            }
        }

        private async Task GenreIsExistAsync(BookDto dto)
        {
            var isExist = await _context.Genre.AnyAsync(w => w.Id == dto.AuthorId);

            if (!isExist)
            {
                var props = new List<string>
                {
                    nameof(BookDto.GenreName),
                    nameof(BookDto.GenreId)
                };

                AddError(_resource.GenreNotFound, props);
            }
        }
    }
}