﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Samples.Domain.Validators.Exceptions;

namespace Samples.Domain.Validators
{
    public abstract class ValidatorBase
    {
        protected ValidatorBase()
        {
            ValidationResults = new List<ValidationResult>();
        }
        
        public List<ValidationResult> ValidationResults { get; set; }

        public bool IsValid => !ValidationResults.Any();

        public void AddError(string message)
        {
            var validationResult = new ValidationResult(message);

            ValidationResults.Add(validationResult);
        }

        public void AddError(string message, string propertyName)
        {
            var validationResult = new ValidationResult(message, new[] { propertyName });

            ValidationResults.Add(validationResult);
        }

        public void AddError(string message, IEnumerable<string> propertyNames)
        {
            var validationResult = new ValidationResult(message, propertyNames);

            ValidationResults.Add(validationResult);
        }

        public void EnsureIsValid()
        {
            if (IsValid) return;

            throw new CommonValidationException(ValidationResults);
        }
    }
}