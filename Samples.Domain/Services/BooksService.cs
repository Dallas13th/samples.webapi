﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Samples.DataAccess.Contexts;
using Samples.DataAccess.GeneratedContext;
using Samples.Domain.Extensions;
using Samples.Domain.Models.Dto;
using Samples.Domain.Models.Filters;
using Samples.Domain.Validators;

namespace Samples.Domain.Services
{
    public sealed class BooksService
    {
        private readonly SamplesContext _context;
        private readonly ILogger<AuthorsService> _logger;
        private readonly BookValidator _validator;

        public BooksService(SamplesContext context, ILogger<AuthorsService> logger, BookValidator validator)
        {
            _context = context;
            _logger = logger;
            _validator = validator;
        }

        public async Task<List<BookDto>> GetBooksAsync(BooksFilter filter)
        {
            var content = await GetBooksQueryable(filter).ToListAsync();

            return content;
        }
        
        public async Task<BookDto> CreateBookAsync(BookDto dto)
        {
            await _validator.ValidateCreationAsync(dto);

            var newBook = new Book
            {
                Name = dto.Name,
                ReleaseDate = dto.ReleaseDate,
                AuthorId = dto.AuthorId,
                GenreId = dto.GenreId
            };

            _context.Book.Add(newBook);

            await _context.SaveChangesAsync();
            
            dto.Id = newBook.Id;

            _logger.LogInformation($"Book created: {dto.Id} {dto.Name}");

            return dto;
        }

        public async Task<BookDto> UpdateBookAsync(BookDto dto)
        {
            await _validator.ValidateUpdatingAsync(dto);

            var book = await _context.Book.FirstAsync(w => w.Id == dto.Id);

            book.Name = dto.Name;
            book.AuthorId = dto.AuthorId;
            book.GenreId = dto.GenreId;
            book.ReleaseDate = dto.ReleaseDate;

            await _context.SaveChangesAsync();

            _logger.LogInformation($"Book updated: {dto.Id} {dto.Name}");

            return dto;
        }

        public async Task<bool> DeleteBookAsync(int id)
        {
            await _validator.ValidateDeletingAsync(id);

            var book = await _context.Book.FirstOrDefaultAsync(w => w.Id == id);

            _context.Book.Remove(book);

            await _context.SaveChangesAsync();

            _logger.LogInformation($"Book created: {book.Id} {book.Name}");

            return true;
        }

        internal IQueryable<BookDto> GetBooksQueryable(BooksFilter filter)
        {
            if (filter == null) filter = new BooksFilter();

            var books = _context.Book;

            var byId = filter.Id.HasValue
                ? books.Where(w => w.Id == filter.Id)
                : books;

            var byName = !string.IsNullOrWhiteSpace(filter.Name)
                ? byId.Where(w => w.Name.Contains(filter.Name))
                : byId;

            var byAuthorId = filter.AuthorId.HasValue
                ? byName.Where(w => w.AuthorId == filter.AuthorId)
                : byName;

            var byGenreId = filter.GenreId.HasValue
                ? byAuthorId.Where(w => w.GenreId == filter.GenreId)
                : byAuthorId;

            var byReleaseDateFrom = filter.ReleaseDateFrom.HasValue
                ? byGenreId.Where(w => w.ReleaseDate >= filter.ReleaseDateFrom)
                : byGenreId;

            var byReleaseDateTo = filter.ReleaseDateTo.HasValue
                ? byReleaseDateFrom.Where(w => w.ReleaseDate <= filter.ReleaseDateTo)
                : byReleaseDateFrom;

            var result = byReleaseDateTo
                .Select(s => new BookDto
                {
                    Id = s.Id,
                    Name = s.Name,
                    AuthorId = s.AuthorId,
                    AuthorName = s.Author.FullName,
                    GenreId = s.GenreId,
                    GenreName = s.Genre.Name,
                    ReleaseDate = s.ReleaseDate
                })
                .Ordered(filter.SortOptions)
                .Paged(filter.Skip, filter.Take);

            return result;
        }
    }
}