﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Samples.DataAccess.Contexts;
using Samples.DataAccess.GeneratedContext;
using Samples.Domain.Extensions;
using Samples.Domain.Models.Dto;
using Samples.Domain.Models.Filters;
using Samples.Domain.Validators;

namespace Samples.Domain.Services
{
    public class AuthorsService
    {
        private readonly SamplesContext _context;
        private readonly ILogger<AuthorsService> _logger;
        private readonly AuthorValidator _validator;

        public AuthorsService(SamplesContext context, ILogger<AuthorsService> logger, AuthorValidator validator)
        {
            _context = context;
            _logger = logger;
            _validator = validator;
        }

        public async Task<List<SimpleDto>> GetAuthorsAsync(BaseFilter filter)
        {
            var content = await GetAuthorsQueryable(filter).ToListAsync();

            return content;
        }
        
        public async Task<SimpleDto> CreateAuthorAsync(SimpleDto dto)
        {
            await _validator.ValidateCreationAsync(dto);

            var newAuthor = new Author
            {
                FullName = dto.Name
            };

            _context.Author.Add(newAuthor);

            await _context.SaveChangesAsync();
            
            dto.Id = newAuthor.Id;

            _logger.LogInformation($"Author created: {dto.Id} {dto.Name}");

            return dto;
        }

        public async Task<SimpleDto> UpdateAuthorAsync(SimpleDto dto)
        {
            await _validator.ValidateUpdatingAsync(dto);
            
            var author = await _context.Author.FirstAsync(w => w.Id == dto.Id);
            
            author.FullName = dto.Name;

            await _context.SaveChangesAsync();

            _logger.LogInformation($"Author updated: {dto.Id} {dto.Name}");

            return dto;
        }

        public async Task<bool> DeleteAuthorAsync(int id)
        {
            await _validator.ValidateDeletingAsync(id);

            await _context.SaveChangesAsync();

            _logger.LogInformation($"Author deleted: Id={id}");

            return true;
        }

        internal IQueryable<SimpleDto> GetAuthorsQueryable(BaseFilter filter)
        {
            if (filter == null)
            {
                filter = new BaseFilter();
            }

            var authors = _context.Author;

            var byId = filter.Id.HasValue
                ? authors.Where(w => w.Id == filter.Id)
                : authors;

            var byName = !string.IsNullOrWhiteSpace(filter.Name)
                ? byId.Where(w => w.FullName.Contains(filter.Name))
                : byId;

            var result = byName
                .Select(s => new SimpleDto
                {
                    Id = s.Id,
                    Name = s.FullName
                })
                .Ordered(filter.SortOptions)
                .Paged(filter.Skip, filter.Take);

            return result;
        }
    }
}