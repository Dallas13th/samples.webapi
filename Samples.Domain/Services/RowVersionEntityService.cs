﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntityFrameworkExtensions;
using Microsoft.EntityFrameworkCore;
using Samples.DataAccess.Contexts;
using Samples.DataAccess.GeneratedContext;
using Samples.Domain.Models.Dto;

namespace Samples.Domain.Services
{
    public class RowVersionEntityService
    {
        private readonly SamplesContext _context;

        public RowVersionEntityService(SamplesContext context)
        {
            _context = context;
        }

        public async Task<RowVersionEntitiesResultDto> GetAllEntitiesByRowVersionAsync(long rowVersionLong)
        {
            var rowVersionFilter = rowVersionLong.Int64ToRowVersionBytes();

            var entities = await _context.DataTypesTestTable
                .Where(w => StructuralComparisons.StructuralComparer.Compare(w.RowVersion, rowVersionFilter) > 0)
                .ToListAsync();

            var resultEntities = entities
                .Select(s => new RowVersionEntityDto
                {
                    Id = s.Id,
                    RowVersionValue = s.RowVersion,
                    DateTimeOffsetValue = s.DateTimeOffsetValue,
                    RowVersionAsLongValue = s.RowVersion.RowVersionToInt64()
                })
                .ToList();

            var currentRowVersion = await _context.GetCurrentRowVersionAsync();

            var result = new RowVersionEntitiesResultDto
            {
                RowVersionEntities = resultEntities,
                CurrentRowVersion = currentRowVersion.RowVersionToInt64()
            };

            return result;
        }
        
        public async Task<RowVersionEntityDto> CreateRowVersionEntityAsync(DateTimeOffset dateTimeOffset)
        {
            var entity = new DataTypesTestTable
            {
                DateTimeOffsetValue = dateTimeOffset
            };

            await _context.DataTypesTestTable.AddAsync(entity);

            await _context.SaveChangesAsync();

            var result = new RowVersionEntityDto
            {
                Id = entity.Id,
                RowVersionValue = entity.RowVersion,
                DateTimeOffsetValue = entity.DateTimeOffsetValue,
                RowVersionAsLongValue = entity.RowVersion.RowVersionToInt64()
            };

            return result;
        }

        public async Task<RowVersionEntityDto> UpdateRowVersionEntityAsync(RowVersionEntityDto dto)
        {
            var editable = await _context.DataTypesTestTable
                .FirstOrDefaultAsync(w => w.Id == dto.Id);

            if (editable == null)
                throw new Exception("Wrong entity id");

            editable.DateTimeOffsetValue = dto.DateTimeOffsetValue;

            await _context.SaveChangesAsync();

            return dto;
        }

        public async Task DeleteRowVersionEntityAsync(int id)
        {
            var entity = await _context.DataTypesTestTable
                .FirstOrDefaultAsync(w => w.Id == id);
            
            if (entity == null)
                return;

            _context.DataTypesTestTable.Remove(entity);

            await _context.SaveChangesAsync();
        }
    }
}