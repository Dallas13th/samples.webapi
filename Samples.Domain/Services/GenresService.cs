﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Samples.DataAccess.Contexts;
using Samples.DataAccess.GeneratedContext;
using Samples.Domain.Extensions;
using Samples.Domain.Models.Dto;
using Samples.Domain.Models.Filters;
using Samples.Domain.Validators;

namespace Samples.Domain.Services
{
    public class GenresService
    {
        private readonly SamplesContext _context;
        private readonly ILogger<AuthorsService> _logger;
        private readonly GenreValidator _validator;

        public GenresService(SamplesContext context, ILogger<AuthorsService> logger, GenreValidator validator)
        {
            _context = context;
            _logger = logger;
            _validator = validator;
        }

        public async Task<List<SimpleDto>> GetGenresAsync(BaseFilter filter)
        {
            var content = await GetGenresQueryable(filter).ToListAsync();

            return content;
        }
        
        public async Task<SimpleDto> CreateGenreAsync(SimpleDto dto)
        {
            await _validator.ValidateCreationAsync(dto);

            var newGenre = new Genre
            {
                Name = dto.Name
            };

            _context.Genre.Add(newGenre);

            await _context.SaveChangesAsync();

            dto.Id = newGenre.Id;

            _logger.LogInformation($"Genre created: {dto.Id} {dto.Name}");
            
            return dto;
        }

        public async Task<SimpleDto> UpdateGenreAsync(SimpleDto dto)
        {
            await _validator.ValidateUpdatingAsync(dto);

            var genre = await _context.Genre.FirstAsync(w => w.Id == dto.Id);
            
            genre.Name = dto.Name;

            await _context.SaveChangesAsync();

            _logger.LogInformation($"Genre updated: {dto.Id} {dto.Name}");

            return dto;
        }

        public async Task<bool> DeleteGenreAsync(int id)
        {
            await _validator.ValidateDeletingAsync(id);

            var genre = await _context.Genre.FirstOrDefaultAsync(w => w.Id == id);

            _context.Genre.Remove(genre);

            await _context.SaveChangesAsync();

            _logger.LogInformation($"Genre updated: {genre.Id} {genre.Name}");

            return true;
        }

        internal IQueryable<SimpleDto> GetGenresQueryable(BaseFilter filter)
        {
            if (filter == null)
            {
                filter = new BaseFilter();
            }

            var genres = _context.Genre;

            var byId = filter.Id.HasValue
                ? genres.Where(w => w.Id == filter.Id)
                : genres;

            var byName = !string.IsNullOrWhiteSpace(filter.Name)
                ? byId.Where(w => w.Name.Contains(filter.Name))
                : byId;

            var result = byName
                .Select(s => new SimpleDto
                {
                    Id = s.Id,
                    Name = s.Name
                })
                .Ordered(filter.SortOptions)
                .Paged(filter.Skip, filter.Take);

            return result;
        }
    }
}