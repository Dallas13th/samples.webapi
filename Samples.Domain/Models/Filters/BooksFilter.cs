﻿using System;

namespace Samples.Domain.Models.Filters
{
    /// <inheritdoc />
    public sealed class BooksFilter : BaseFilter
    {
        /// <summary>
        /// Author Id
        /// </summary>
        public int? AuthorId { get; set; }

        /// <summary>
        /// Genre Id
        /// </summary>
        public int? GenreId { get; set; }

        /// <summary>
        /// Release date From
        /// </summary>
        public DateTime? ReleaseDateFrom { get; set; }

        /// <summary>
        /// Release date To
        /// </summary>
        public DateTime? ReleaseDateTo { get; set; }
    }
}