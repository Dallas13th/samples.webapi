﻿using System.Collections.Generic;
using EntityFrameworkExtensions.Models;

namespace Samples.Domain.Models.Filters
{
    /// <summary>
    /// Filtering model
    /// </summary>
    public class BaseFilter
    {
        /// <summary>
        /// Id
        /// </summary>
        public int? Id { get; set; }
        
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Skip rows
        /// </summary>
        public int? Skip { get; set; }

        /// <summary>
        /// Take rows
        /// </summary>
        public int? Take { get; set; }

        /// <summary>
        /// <inheritdoc cref="SortOption"/>
        /// </summary>
        public List<SortOption> SortOptions { get; set; }
    }
}