﻿using System.Collections.Generic;

namespace Samples.Domain.Models.Dto
{
    public sealed class RowVersionEntitiesResultDto
    {
        public RowVersionEntitiesResultDto()
        {
            RowVersionEntities = new List<RowVersionEntityDto>();
        }

        public long CurrentRowVersion { get; set; }

        public List<RowVersionEntityDto> RowVersionEntities { get; set; }
    }
}