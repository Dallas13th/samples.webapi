﻿using System;

namespace Samples.Domain.Models.Dto
{
    public sealed class RowVersionEntityDto
    {
        public int Id { get; set; }

        public DateTimeOffset DateTimeOffsetValue { get; set; }

        public byte[] RowVersionValue { get; set; }

        public long RowVersionAsLongValue { get; set; }
    }
}