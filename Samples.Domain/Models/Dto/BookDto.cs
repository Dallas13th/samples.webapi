﻿using System;

namespace Samples.Domain.Models.Dto
{
    /// <summary>
    /// Book Dto
    /// </summary>
    public sealed class BookDto
    {
        /// <summary>
        /// Book Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Book Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Author Id
        /// </summary>
        public int AuthorId { get; set; }

        /// <summary>
        /// Author Name
        /// </summary>
        public string AuthorName { get; set; }

        /// <summary>
        /// Genre Id
        /// </summary>
        public int GenreId { get; set; }

        /// <summary>
        /// Genre Name
        /// </summary>
        public string GenreName { get; set; }

        /// <summary>
        /// Release Date
        /// </summary>
        public DateTime ReleaseDate { get; set; }
    }
}