﻿namespace Samples.Domain.Models.Dto
{
    /// <summary>
    /// Simple dto for transfering id with name pairs
    /// </summary>
    public sealed class SimpleDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}