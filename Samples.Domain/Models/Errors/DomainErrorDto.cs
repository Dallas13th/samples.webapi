﻿using System.Collections.Generic;

namespace Samples.Domain.Models.Errors
{
    /// <summary>
    /// Error Dto
    /// </summary>
    public class DomainErrorDto
    {
        public DomainErrorDto()
        {
            FieldNames = new string[0];
        }

        /// <summary>
        /// User localized message
        /// </summary>
        public string UserMessage { get; set; }

        /// <summary>
        /// Field names if existing
        /// </summary>
        public IEnumerable<string> FieldNames { get; set; }
        
        /// <inheritdoc cref="DomainErrorInfo"/>
        public DomainErrorInfo ErrorInfo { get; set; }
    }
}