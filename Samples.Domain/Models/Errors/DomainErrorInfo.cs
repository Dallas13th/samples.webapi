﻿namespace Samples.Domain.Models.Errors
{
    /// <summary>
    /// Domain error system info
    /// </summary>
    public sealed class DomainErrorInfo
    {
        /// <summary>
        /// System message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// System stack trace
        /// </summary>
        public string StackTrace { get; set; }
        
        /// <inheritdoc cref="DomainErrorDto"/>
        public DomainErrorDto InnerError { get; set; }
    }
}