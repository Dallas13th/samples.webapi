﻿using System.Collections.Generic;
using System.Linq;
using EntityFrameworkExtensions;
using EntityFrameworkExtensions.Models;

namespace Samples.Domain.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<TModel> Ordered<TModel>(this IQueryable<TModel> source, List<SortOption> sortOptions)
        {
            return sortOptions != null && sortOptions.Any() ? source.OrderBy(sortOptions) : source;
        }

        public static IQueryable<TModel> Paged<TModel>(this IQueryable<TModel> source, int? skip, int? take)
        {
            var skipResult = skip.HasValue
                ? source.Skip(skip.Value)
                : source;

            var takeResult = take.HasValue
                ? skipResult.Take(take.Value)
                : skipResult;

            return takeResult;
        }
    }
}