﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using EntityFrameworkExtensions.Attributes;
using EntityFrameworkExtensions.Builders;
using EntityFrameworkExtensions.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Data.SqlClient.Server;

namespace EntityFrameworkExtensions
{
    public static class TableValuedParameterExtensions
    {
        public static SqlParameter ToTableValuedParameter<TModel>(this IEnumerable<TModel> source, string parameterName)
        {
            if (source == null) return null;

            var elementType = typeof(TModel);

            var tvpAttribute = elementType
                .GetTypeInfo()
                .GetCustomAttribute<SqlTableValuedParameterAttribute>();

            var typeName = tvpAttribute != null ? tvpAttribute.TypeName : elementType.Name;

            var sqlProps = CreateMetaData(elementType);

            var sqlRecords = source
                .Select(s => new SqlDataRecordBuilder<TModel>(s, sqlProps).Build())
                .ToList();

            var result = new SqlParameter
            {
                ParameterName = parameterName,
                Value = sqlRecords,
                TypeName = typeName,
                SqlDbType = SqlDbType.Structured
            };

            return result;
        }

        private static SqlTypedProperty[] CreateMetaData(Type elementType)
        {
            var elementProps = elementType.GetProperties();

            var sqlProps = new List<SqlTypedProperty>();

            foreach (var elementProp in elementProps)
            {
                var sqlTypeAttribute = elementProp.GetCustomAttribute<SqlDbTypeAttribute>();
                var sqlElementType = sqlTypeAttribute?.Type ?? GetSqlDbType(elementProp.PropertyType);
                
                var metadataItem = sqlElementType == SqlDbType.NVarChar
                    ? new SqlMetaData(elementProp.Name, sqlElementType, sqlTypeAttribute?.VarCharMaxLength ?? 4000)
                    : new SqlMetaData(elementProp.Name, sqlElementType);

                var propWithMetaData = new SqlTypedProperty(elementProp, metadataItem);

                sqlProps.Add(propWithMetaData);
            }

            return sqlProps.ToArray();
        }

        private static SqlDbType GetSqlDbType(Type propertyType)
        {
            if (propertyType == typeof(Guid)) return SqlDbType.UniqueIdentifier;
            if (propertyType == typeof(Guid?)) return SqlDbType.UniqueIdentifier;
            if (propertyType == typeof(int)) return SqlDbType.Int;
            if (propertyType == typeof(int?)) return SqlDbType.Int;
            if (propertyType == typeof(string)) return SqlDbType.NVarChar;
            if (propertyType == typeof(decimal)) return SqlDbType.Decimal;
            if (propertyType == typeof(decimal?)) return SqlDbType.Decimal;
            if (propertyType == typeof(DateTime)) return SqlDbType.DateTime2;
            if (propertyType == typeof(DateTime?)) return SqlDbType.DateTime2;
            if (propertyType == typeof(DateTimeOffset)) return SqlDbType.DateTimeOffset;
            if (propertyType == typeof(DateTimeOffset?)) return SqlDbType.DateTimeOffset;

            var msg = $"{propertyType.FullName} is not supported as element of IEnumerable<> for TableValuedParameter";

            throw new InvalidOperationException(msg);
        }
    }
}