﻿using System;

namespace EntityFrameworkExtensions.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SqlTableValuedParameterAttribute : Attribute
    {
        public SqlTableValuedParameterAttribute(string typeName)
        {
            TypeName = typeName;
        }

        public string TypeName { get; }
    }
}