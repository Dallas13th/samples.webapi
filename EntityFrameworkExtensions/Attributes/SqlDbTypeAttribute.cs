﻿using System;
using System.Data;

namespace EntityFrameworkExtensions.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class SqlDbTypeAttribute : Attribute
    {
        public SqlDbTypeAttribute(SqlDbType type)
        {
            Type = type;
        }

        public SqlDbType Type { get; }
        
        public long VarCharMaxLength { get; set; }
    }
}