﻿using System;

namespace EntityFrameworkExtensions.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SortOptionAttribute : Attribute
    {
        public SortOptionAttribute(string propertyName)
        {
            PropertyName = propertyName;
        }

        public string PropertyName { get; }
    }
}