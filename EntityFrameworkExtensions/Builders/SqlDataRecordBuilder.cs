﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using EntityFrameworkExtensions.Models;
using Microsoft.Data.SqlClient.Server;

namespace EntityFrameworkExtensions.Builders
{
    internal sealed class SqlDataRecordBuilder<TModel>
    {
        public SqlDataRecordBuilder(TModel item, SqlTypedProperty[] properties)
        {
            Properties = properties;
            Item = item;

            Record = new SqlDataRecord(properties.Select(s => s.MetaData).ToArray());
        }
        
        public SqlTypedProperty[] Properties { get; }

        public SqlDataRecord Record { get; set; }

        public TModel Item { get; set; }
        
        public SqlDataRecord Build()
        {
            var i = 0;
            foreach (var prop in Properties)
            {
                var propValue = prop.Property.GetValue(Item);
                SetDataRecordValue(i, propValue, prop.MetaData.SqlDbType);
                i++;
            }

            return Record;
        }

        private void SetGuidValue(int index, object value)
        {
            var val = (Guid?) value;

            if (val.HasValue)
            {
                Record.SetGuid(index, val.Value);
                return;
            }

            Record.SetDBNull(index);
        }
        
        private void SetIntValue(int index, object value)
        {
            var val = (int?) value;

            if (val.HasValue)
            {
                Record.SetInt32(index, val.Value);
                return;
            }

            Record.SetDBNull(index);
        }

        private void SetStringValue(int index, object value)
        {
            var val = (string) value;

            if (val != null)
            {
                Record.SetString(index, val);
                return;
            }

            Record.SetDBNull(index);
        }

        private void SetDecimalValue(int index, object value)
        {
            var val = (decimal?) value;

            if (val.HasValue)
            {
                Record.SetDecimal(index, val.Value);
                return;
            }

            Record.SetDBNull(index);
        }

        private void SetSqlMoneyValue(int index, object value)
        {
            var val = (decimal?)value;

            if (val.HasValue)
            {
                Record.SetSqlMoney(index, new SqlMoney(val.Value));
                return;
            }

            Record.SetDBNull(index);
        }

        private void SetDateTimeValue(int index, object value)
        {
            var val = (DateTime?) value;

            if (val.HasValue)
            {
                Record.SetDateTime(index, val.Value);
                return;
            }

            Record.SetDBNull(index);
        }

        private void SetDateTimeOffsetValue(int index, object value)
        {
            var val = (DateTimeOffset?) value;

            if (val.HasValue)
            {
                Record.SetDateTimeOffset(index, val.Value);
                return;
            }

            Record.SetDBNull(index);
        }
        
        private void SetDataRecordValue(int index, object value, SqlDbType sqlType)
        {
            switch (sqlType)
            {
                case SqlDbType.Int:
                    SetIntValue(index, value);
                    break;
                case SqlDbType.UniqueIdentifier:
                    SetGuidValue(index, value);
                    break;
                case SqlDbType.NVarChar:
                    SetStringValue(index, value);
                    break;
                case SqlDbType.Decimal:
                    SetDecimalValue(index, value);
                    break;
                case SqlDbType.DateTime2:
                    SetDateTimeValue(index, value);
                    break;
                case SqlDbType.DateTimeOffset:
                    SetDateTimeOffsetValue(index, value);
                    break;
                case SqlDbType.Money:
                    SetSqlMoneyValue(index, value);
                    break;

                default:
                    throw new InvalidOperationException("Unsupported property type of TVP");
            }
        }
    }
}