﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using EntityFrameworkExtensions.Attributes;
using EntityFrameworkExtensions.Models;

namespace EntityFrameworkExtensions
{
    public static class OrderByExtensions
    {
        public static IEnumerable<SortOption> GetAttributeCorrectedProperties<TModel>(this IEnumerable<SortOption> sortOptions)
        {
            var sortOptionsList = sortOptions.ToList();

            var modelProperties = typeof(TModel)
                .GetProperties()
                .Select(s => new
                {
                    Property = s,
                    Attribute = s.GetCustomAttribute<SortOptionAttribute>()
                })
                .Where(w => w.Attribute != null)
                .ToList();
            
            foreach (var sortOption in sortOptionsList)
            {
                var property = modelProperties.FirstOrDefault(w => w.Property.Name == sortOption.ColumnName);

                if (property == null) continue;

                sortOption.ColumnName = property.Attribute.PropertyName;
            }

            return sortOptionsList;
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, IEnumerable<SortOption> sortOptions)
        {
            IOrderedQueryable<T> result = null;

            foreach (var sortOption in sortOptions)
            {
                result = OrderingHelper(result ?? source, sortOption.ColumnName, sortOption.IsDescending, result != null);
            }

            return result;
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, false, false);
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string propertyName, bool descending)
        {
            return OrderingHelper(source, propertyName, descending, false);
        }

        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, false, true);
        }

        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string propertyName, bool descending)
        {
            return OrderingHelper(source, propertyName, descending, true);
        }

        private static IOrderedQueryable<T> OrderingHelper<T>(IQueryable<T> source, string propertyName, bool descending, bool anotherLevel)
        {
            var param = Expression.Parameter(typeof(T), "p");
            var property = Expression.PropertyOrField(param, propertyName);
            var sort = Expression.Lambda(property, param);

            var method = anotherLevel ? "ThenBy" : "OrderBy";
            var direction = descending ? "Descending" : string.Empty;

            var call = Expression.Call(typeof(Queryable),
                method + direction,
                new[] {typeof(T), property.Type},
                source.Expression,
                Expression.Quote(sort));

            return (IOrderedQueryable<T>) source.Provider.CreateQuery<T>(call);
        }
    }
}