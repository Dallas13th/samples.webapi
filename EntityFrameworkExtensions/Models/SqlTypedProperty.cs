﻿using System.Reflection;
using Microsoft.Data.SqlClient.Server;

namespace EntityFrameworkExtensions.Models
{
    internal sealed class SqlTypedProperty
    {
        public SqlTypedProperty(PropertyInfo property, SqlMetaData metaData)
        {
            MetaData = metaData;
            Property = property;
        }
        
        public SqlMetaData MetaData { get; }

        public PropertyInfo Property { get; }
    }
}