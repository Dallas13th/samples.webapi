﻿namespace EntityFrameworkExtensions.Models
{
    /// <summary>
    /// Sort option
    /// </summary>
    public sealed class SortOption
    {
        /// <summary>
        /// Column Name
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// Is descending
        /// </summary>
        public bool IsDescending { get; set; }
    }
}