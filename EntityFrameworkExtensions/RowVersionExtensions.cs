﻿using System;
using System.Linq;

namespace EntityFrameworkExtensions
{
    public static class RowVersionExtensions
    {
        public static long RowVersionToInt64(this byte[] rowVersion)
        {
            if (rowVersion.Length != 8)
                throw new InvalidOperationException("Expected 8 bytes int64 value");

            var reversed = rowVersion.Reverse().ToArray();

            var result = BitConverter.ToInt64(reversed);

            return result;
        }

        public static byte[] Int64ToRowVersionBytes(this long rowVersion)
        {
            var bytes = BitConverter.GetBytes(rowVersion);

            var result = bytes.Reverse().ToArray();

            return result;
        }
    }
}