﻿using System.Collections.Generic;
using Samples.DataAccess.UserDefinedTypes;

namespace Samples.DataAccess.IntegrationTests.Comparers
{
    public sealed class CommonTableTypeComparer : IEqualityComparer<CommonTableType>
    {
        public bool Equals(CommonTableType x, CommonTableType y)
        {
            if (x == null && y == null) return true;
            if (x == null || y == null) return false;

            if (x.IntValue != y.IntValue) return false;
            if (x.DateTimeValue != y.DateTimeValue) return false;
            if (x.DateTimeValue != y.DateTimeValue) return false;
            if (x.DecimalValue != y.DecimalValue) return false;
            if (x.GuidValue != y.GuidValue) return false;
            if (x.MoneyValue != y.MoneyValue) return false;
            if (x.StringValue != y.StringValue) return false;
            if (x.DateTimeOffsetValue != y.DateTimeOffsetValue) return false;

            return true;
        }

        public int GetHashCode(CommonTableType obj)
        {
            return obj.GetHashCode();
        }
    }
}