﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Samples.DataAccess.IntegrationTests.Comparers;
using Samples.DataAccess.IntegrationTests.Mocks;
using Xunit;

namespace Samples.DataAccess.IntegrationTests
{
    public sealed class StoredProceduresTest : IntegrationTestBase
    {
        [Fact]
        public async Task EchoTvpProcedure_ShouldReturn_SameCollection()
        {
            var expected = TvpContentMock.CreateCommonTableTypes()
                .OrderBy(o => o.IntValue)
                .ToList();

            var queryResult = await Context.EchoTvpProcedure(expected)
                .ToListAsync();

            var sortedResult = queryResult
                .OrderBy(o => o.IntValue)
                .ToList();

            Assert.Equal(expected, sortedResult, new CommonTableTypeComparer());
        }

        [Fact]
        public async Task InnerJoinWithStoredProcedure_ShouldThrow_InvalidOperation()
        {
            var procedureQuery = Context.GetBooksProcedure();

            var joined = Context.Book
                .Join(procedureQuery,
                    t => t.Id,
                    p => p.BookId,
                    (t, p) => new
                    {
                        ResultFromTable = t,
                        ResultFromProcedure = p
                    })
                .Where(w => w.ResultFromProcedure.BookName != "Not existing"
                            && w.ResultFromTable.Author.FullName != "Not exisitng")
                .OrderBy(o => o.ResultFromProcedure.AuthorName)
                .ThenBy(t => t.ResultFromTable.Name)
                .Skip(1)
                .Take(2);
            
            //EFCore does not support join with procedure anymore
            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                await joined.ToListAsync();
            });
        }
    }
}