﻿using System;
using System.Collections.Generic;
using System.Linq;
using Samples.DataAccess.UserDefinedTypes;

namespace Samples.DataAccess.IntegrationTests.Mocks
{
    public static class TvpContentMock
    {
        public static List<CommonTableType> CreateCommonTableTypes()
        {
            var result = Enumerable.Range(0, 10)
                .Select(s => new CommonTableType
                {
                    IntValue = s,
                    GuidValue = Guid.NewGuid(),
                    DateTimeValue = DateTime.Now,
                    DecimalValue = s,
                    StringValue = "Sample string",
                    MoneyValue = s + (decimal)s / 10,
                    DateTimeOffsetValue = DateTimeOffset.Now
                })
                .ToList();

            return result;
        }
    }
}