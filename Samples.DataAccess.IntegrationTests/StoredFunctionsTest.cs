﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Samples.DataAccess.IntegrationTests.Comparers;
using Samples.DataAccess.IntegrationTests.Mocks;
using Xunit;

namespace Samples.DataAccess.IntegrationTests
{
    public sealed class StoredFunctionsTest : IntegrationTestBase
    {
        [Fact]
        public async Task StringSplit_ShouldReturn_TableOfIntegers()
        {
            const string separator = ",";
            var expectedArray = new List<int> {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            var arrayString = string.Join(separator, expectedArray);

            var result = await Context.SplitStringToInts(arrayString, separator)
                .Select(s => s.IntValue)
                .ToListAsync();

            Assert.Equal(expectedArray.AsEnumerable(), result);
        }

        [Fact]
        public async Task EchoTvpFunction_ShouldReturn_SameCollection()
        {
            var expected = TvpContentMock.CreateCommonTableTypes()
                .OrderBy(o => o.IntValue)
                .ToList();

            var result = await Context.EchoTvpFunction(expected)
                .OrderBy(o => o.IntValue)
                .ToListAsync();

            Assert.Equal(expected, result, new CommonTableTypeComparer());
        }

        [Fact]
        public async Task InnerJoinWithStoredFunction_ShouldHave_NotEmptyProperties()
        {
            var tvpContent = TvpContentMock.CreateCommonTableTypes()
                .ToList();
            
            var functionQuery = Context.EchoTvpFunction(tvpContent)
                .OrderBy(o => o.IntValue);

            var result = Context.Author
                .Join(functionQuery,
                    t => t.Id,
                    p => p.IntValue,
                    (t, p) => new
                    {
                        ResultFromTable = t.FullName,
                        ResultFromProcedure = p.StringValue
                    });

            bool allElementsIsNotEmpty = await result
                .AllAsync(w => w.ResultFromTable != null && w.ResultFromProcedure != null);

            Assert.True(allElementsIsNotEmpty);
        }
    }
}