using System.IO;
using Microsoft.Extensions.Configuration;
using Samples.DataAccess.Contexts;

namespace Samples.DataAccess.IntegrationTests
{
    public abstract class IntegrationTestBase
    {
        static IntegrationTestBase()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, false);

            var configuration = builder.Build();

            ConnectionString = configuration.GetConnectionString("SamplesDataBase");
        }

        protected IntegrationTestBase()
        {
            Context = new SamplesContext(ConnectionString);

            SqliteContext = new SamplesSqliteContext("Data Source=SamplesDataBase.sqlite3");
        }

        public SamplesContext Context { get; }

        public SamplesSqliteContext SqliteContext { get; set; }
        
        public static string ConnectionString { get; set; }
    }
}