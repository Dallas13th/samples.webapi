﻿using System;
using System.Linq;
using EntityFrameworkExtensions;
using Samples.DataAccess.GeneratedContext;

namespace Samples.DataAccess.ContentBuilders
{
    public sealed class DataTypesTestTableBuilder : ITableContentBuilder
    {
        public bool AlreadyFilled { get; set; }

        public void FillContent(SamplesContextBase context)
        {
            var content = Enumerable.Range(0, 10)
                .Select(s => new DataTypesTestTable
                {
                    DateTimeOffsetValue = DateTimeOffset.Now,
                    RowVersion = ((long)s).Int64ToRowVersionBytes()
                });

            context.DataTypesTestTable.AddRange(content);
        }
    }
}