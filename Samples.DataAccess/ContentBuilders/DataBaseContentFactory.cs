﻿using Samples.DataAccess.GeneratedContext;

namespace Samples.DataAccess.ContentBuilders
{
    public static class DataBaseContentFactory
    {
        private static readonly object ContentLocker;

        private static bool _isContentFilled;

        static DataBaseContentFactory()
        {
            ContentLocker = new object();
        }

        public static void CreateContent(SamplesContextBase context)
        {
            var contentBuilder = new DataBaseContentBuilder(context);

            contentBuilder.AddTableContentBuilder(new AuthorContentBuilder());
            contentBuilder.AddTableContentBuilder(new GenreContentBuilder());
            contentBuilder.AddTableContentBuilder(new BookContentBuilder());
            contentBuilder.AddTableContentBuilder(new DataTypesTestTableBuilder());
            contentBuilder.Build();
        }

        public static void EnsureContentFilled(SamplesContextBase context)
        {
            lock (ContentLocker)
            {
                if (_isContentFilled)
                    return;

                CreateContent(context);

                _isContentFilled = true;
            }
        }
    }
}