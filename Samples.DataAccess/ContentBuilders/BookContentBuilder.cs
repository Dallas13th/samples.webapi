﻿using System;
using System.Linq;
using Samples.DataAccess.GeneratedContext;

namespace Samples.DataAccess.ContentBuilders
{
    public sealed class BookContentBuilder : ITableContentBuilder
    {
        public bool AlreadyFilled { get; set; }
        
        public void FillContent(SamplesContextBase context)
        {
            var content = Enumerable.Range(0, 10)
                .Select(s => new Book
                {
                    Name = "BookName_" + s,
                    AuthorId = s,
                    GenreId = s,
                    ReleaseDate = DateTime.Now
                });

            context.Book.AddRange(content);
        }
    }
}