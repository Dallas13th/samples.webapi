﻿using System.Linq;
using Samples.DataAccess.GeneratedContext;

namespace Samples.DataAccess.ContentBuilders
{
    public sealed class AuthorContentBuilder : ITableContentBuilder
    {
        public bool AlreadyFilled { get; set; }

        public void FillContent(SamplesContextBase context)
        {
            var content = Enumerable.Range(0, 10)
                .Select(s => new Author
                {
                    FullName = "AuthorFullName_" + s
                });

            context.Author.AddRange(content);
        }
    }
}