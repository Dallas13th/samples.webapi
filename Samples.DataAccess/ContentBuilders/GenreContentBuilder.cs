﻿using System.Linq;
using Samples.DataAccess.GeneratedContext;

namespace Samples.DataAccess.ContentBuilders
{
    public sealed class GenreContentBuilder : ITableContentBuilder
    {
        public bool AlreadyFilled { get; set; }

        public void FillContent(SamplesContextBase context)
        {
            var content = Enumerable.Range(0, 10)
                .Select(s => new Genre
                {
                    Name = "GenreName_" + s
                });

            context.Genre.AddRange(content);
        }
    }
}