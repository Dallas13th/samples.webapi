﻿using System.Collections.Generic;
using System.Linq;
using Samples.DataAccess.GeneratedContext;

namespace Samples.DataAccess.ContentBuilders
{
    public sealed class DataBaseContentBuilder
    {
        private readonly SamplesContextBase _context;
        private readonly List<ITableContentBuilder> _tableBuilders;

        public DataBaseContentBuilder(SamplesContextBase context)
        {
            _context = context;
            _tableBuilders = new List<ITableContentBuilder>();
        }

        public void AddTableContentBuilder(ITableContentBuilder contentBuilder)
        {
            if (_tableBuilders.Any(w => w.GetType() == contentBuilder.GetType())) return;

            _tableBuilders.Add(contentBuilder);
        }
        
        public SamplesContextBase Build()
        {
            foreach (var tableBuilder in _tableBuilders)
            {
                if (tableBuilder.AlreadyFilled) continue;

                tableBuilder.FillContent(_context);

                tableBuilder.AlreadyFilled = true;
            }

            _context.SaveChanges();
            
            return _context;
        }
    }
}