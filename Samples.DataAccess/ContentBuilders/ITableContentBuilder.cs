﻿using Samples.DataAccess.GeneratedContext;

namespace Samples.DataAccess.ContentBuilders
{
    public interface ITableContentBuilder
    {
        bool AlreadyFilled { get; set; }
        
        void FillContent(SamplesContextBase context);
    }
}