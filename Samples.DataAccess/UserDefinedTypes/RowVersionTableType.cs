﻿namespace Samples.DataAccess.UserDefinedTypes
{
    public sealed class RowVersionTableType
    {
        public byte[] RowVersionValue { get; set; }
    }
}