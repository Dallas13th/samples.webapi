﻿namespace Samples.DataAccess.UserDefinedTypes
{
    public sealed class SplitStringToIntsResult
    {
        public int IntValue { get; set; }

        public string StringValue { get; set; }
    }
}