﻿using System;

namespace Samples.DataAccess.UserDefinedTypes
{
    public sealed class GetBooksProcedureResult
    {
        public int BookId { get; set; }

        public string AuthorName { get; set; }

        public string GenreName { get; set; }

        public string BookName { get; set; }

        public DateTime ReleaseDate { get; set; }
    }
}