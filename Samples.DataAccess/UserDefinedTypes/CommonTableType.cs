﻿using System;
using System.Data;
using EntityFrameworkExtensions.Attributes;

namespace Samples.DataAccess.UserDefinedTypes
{
    public sealed class CommonTableType
    {
        public Guid GuidValue { get; set; }

        public int IntValue { get; set; }

        public string StringValue { get; set; }

        public DateTime DateTimeValue { get; set; }

        public decimal DecimalValue { get; set; }
        
        [SqlDbType(SqlDbType.Money)]
        public decimal MoneyValue { get; set; }

        public DateTimeOffset DateTimeOffsetValue { get; set; }
    }
}