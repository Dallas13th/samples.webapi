﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Samples.DataAccess.GeneratedContext
{
    public partial class Author
    {
        public Author()
        {
            Book = new HashSet<Book>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(600)]
        public string FullName { get; set; }

        [InverseProperty("Author")]
        public ICollection<Book> Book { get; set; }
    }
}
