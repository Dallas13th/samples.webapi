﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Samples.DataAccess.GeneratedContext
{
    public partial class DataTypesTestTable
    {
        public int Id { get; set; }
        public DateTimeOffset DateTimeOffsetValue { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
    }
}
