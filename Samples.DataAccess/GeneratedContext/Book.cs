﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Samples.DataAccess.GeneratedContext
{
    public partial class Book
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int GenreId { get; set; }
        [Required]
        [StringLength(150)]
        public string Name { get; set; }
        public DateTime ReleaseDate { get; set; }

        [ForeignKey("AuthorId")]
        [InverseProperty("Book")]
        public Author Author { get; set; }
        [ForeignKey("GenreId")]
        [InverseProperty("Book")]
        public Genre Genre { get; set; }
    }
}
