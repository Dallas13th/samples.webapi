﻿namespace Samples.DataAccess.Settings
{
    public interface IDbConnectionSettings
    {
        string ConnectionString { get; }

        bool UseInMemoryContext { get; }
    }
}