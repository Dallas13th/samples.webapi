﻿using System;
using Microsoft.EntityFrameworkCore;
using Samples.DataAccess.Contexts;
using Samples.DataAccess.GeneratedContext;

namespace Samples.DataAccess.Settings
{
    public static class ContextOptionsFactory
    {
        public static DbContextOptions<SamplesContextBase> CreateSqlServerOptions(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SamplesContextBase>();

            optionsBuilder.UseSqlServer(connectionString);

            return optionsBuilder.Options;
        }

        public static DbContextOptions<SamplesContextBase> CreateSqlServerOptions(IDbConnectionSettings settings)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SamplesContextBase>();

            if (settings.UseInMemoryContext)
            {
                optionsBuilder.UseInMemoryDatabase(nameof(SamplesInMemoryContext));

                return optionsBuilder.Options;
            }

            optionsBuilder.UseSqlServer(settings.ConnectionString);

            return optionsBuilder.Options;
        }

        public static DbContextOptions<SamplesContextBase> CreateInMemoryDbOptions()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SamplesContextBase>();

            optionsBuilder.UseInMemoryDatabase(nameof(SamplesInMemoryContext) + Guid.NewGuid().ToString("N"));

            return optionsBuilder.Options;
        }

        public static DbContextOptions<SamplesContextBase> CreateSqliteOptions(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SamplesContextBase>();

            optionsBuilder.UseSqlite(connectionString);

            return optionsBuilder.Options;
        }
    }
}