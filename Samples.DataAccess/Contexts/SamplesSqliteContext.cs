﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Samples.DataAccess.GeneratedContext;
using Samples.DataAccess.Settings;

namespace Samples.DataAccess.Contexts
{
    public class SamplesSqliteContext : SamplesContextBase
    {
        private static volatile bool _creationEnsured;
        private static readonly object DbCreationLocker;

        static SamplesSqliteContext()
        {
            DbCreationLocker = new object();
        }

        public SamplesSqliteContext(DbContextOptions<SamplesContextBase> options)
            : base(options)
        {
            lock (DbCreationLocker)
            {
                if (_creationEnsured) return;

                Database.EnsureCreated();
                _creationEnsured = true;
            }
        }

        public SamplesSqliteContext(string connectionString)
            : this(ContextOptionsFactory.CreateSqliteOptions(connectionString))
        {

        }

        public sealed override DatabaseFacade Database => base.Database;
    }
}