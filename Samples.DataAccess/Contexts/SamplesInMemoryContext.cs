﻿using Samples.DataAccess.ContentBuilders;
using Samples.DataAccess.GeneratedContext;
using Samples.DataAccess.Settings;

namespace Samples.DataAccess.Contexts
{
    public class SamplesInMemoryContext : SamplesContextBase
    {
        public SamplesInMemoryContext()
            : base(ContextOptionsFactory.CreateInMemoryDbOptions())
        {
            DataBaseContentFactory.CreateContent(this);
        }
    }
}