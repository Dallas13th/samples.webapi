﻿using System.Collections.Generic;
using System.Linq;
using Samples.Domain.Models.Errors;

namespace Samples.WebApi.Models.Result
{
    /// <summary>
    /// Api result Model
    /// </summary>
    /// <typeparam name="TContent"><inheritdoc cref="Content"/></typeparam>
    public sealed class ApiResult<TContent>
    {
        public ApiResult()
        {
            Errors = new DomainErrorDto[0];
        }

        public ApiResult(TContent content)
            : this()
        {
            Content = content;
        }

        public ApiResult(IEnumerable<DomainErrorDto> errors)
        {
            Errors = errors;
        }

        public ApiResult(DomainErrorDto error)
        {
            Errors = new[] { error };
        }

        public ApiResult(string userMessage)
            : this(userMessage, null)
        {

        }

        public ApiResult(string userMessage, string systemMessage)
        {
            var error = new DomainErrorDto
            {
                UserMessage = userMessage
            };

            if (!string.IsNullOrWhiteSpace(systemMessage))
            {
                error.ErrorInfo = new DomainErrorInfo
                {
                    Message = systemMessage
                };
            }

            Errors = new[] { error };
        }

        /// <summary>
        /// Content
        /// </summary>
        public TContent Content { get; }
        
        /// <summary>
        /// Is Success
        /// </summary>
        public bool IsSuccess => !Errors.Any();

        /// <inheritdoc cref="DomainErrorDto"/>
        public IEnumerable<DomainErrorDto> Errors { get; set; }
    }
}