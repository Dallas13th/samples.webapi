﻿using Microsoft.Extensions.Options;
using Samples.DataAccess.Settings;

namespace Samples.WebApi.AppSettings
{
    public sealed class DbConnectionSettings : IDbConnectionSettings
    {
        public DbConnectionSettings(IOptions<ConnectionSettingsSection> connectionOptions)
        {
            ConnectionString = connectionOptions.Value.SamplesConnectionString;
            UseInMemoryContext = connectionOptions.Value.UseInMemoryContext;
        }

        public string ConnectionString { get; }

        public bool UseInMemoryContext { get; }
    }
}