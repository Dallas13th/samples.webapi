﻿namespace Samples.WebApi.AppSettings
{
    public sealed class ConnectionSettingsSection
    {
        public string SamplesConnectionString { get; set; }

        public bool UseInMemoryContext { get; set; }
    }
}