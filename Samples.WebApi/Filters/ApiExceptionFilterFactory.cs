﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Samples.Domain.Resources.Locale;

namespace Samples.WebApi.Filters
{
    public sealed class ApiExceptionFilterFactory : IFilterFactory
    {
        public bool IsReusable => false;

        public IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
        {
            var logger = serviceProvider.GetRequiredService<ILogger<ApiExceptionFilterAttribute>>();
            
            var env = serviceProvider.GetRequiredService<IWebHostEnvironment>();

            var resource = serviceProvider.GetRequiredService<ICommonResource>();

            return new ApiExceptionFilterAttribute(logger, env, resource);
        }
    }
}