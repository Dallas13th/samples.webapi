﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Samples.Domain.Models.Errors;
using Samples.Domain.Resources.Locale;
using Samples.Domain.Validators.Exceptions;
using Samples.WebApi.Models.Result;

namespace Samples.WebApi.Filters
{
    public sealed class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger<ApiExceptionFilterAttribute> _logger;
        private readonly IWebHostEnvironment _environment;
        private readonly ICommonResource _resource;

        public ApiExceptionFilterAttribute(ILogger<ApiExceptionFilterAttribute> logger,
            IWebHostEnvironment environment,
            ICommonResource resource)
        {
            _logger = logger;
            _environment = environment;
            _resource = resource;
        }
        
        public override void OnException(ExceptionContext context)
        {
            context.Result = CreateResult(context.Exception);
        }

        private ObjectResult CreateResult(Exception ex)
        {
            if (ex is CommonValidationException validationException)
            {
                var domainErrors = new List<DomainErrorDto>();
                foreach (var validationError in validationException.ValidationResults)
                {
                    var domainError = new DomainErrorDto
                    {
                        UserMessage = validationError.ErrorMessage,
                        FieldNames = validationError.MemberNames
                    };

                    domainErrors.Add(domainError);
                }

                var validationApiResult = new ApiResult<object>(domainErrors);

                var validationResult = new ObjectResult(validationApiResult)
                {
                    StatusCode = 400
                };

                return validationResult;
            }

            var errorDto = CreateErrorDto(ex, _resource.UnhandledErrorOccured, _environment.IsDevelopment());

            var businessResult = new ApiResult<object>(errorDto);

            var apiResult = new ObjectResult(businessResult)
            {
                StatusCode = 520
            };

            _logger.LogError(ex, _resource.UnhandledErrorOccured);

            return apiResult;
        }

        //TODO: debug this
        private static DomainErrorDto CreateErrorDto(Exception exception, string userMessage, bool showSystemErrors)
        {
            var currentException = exception;

            DomainErrorDto resultError = null;
            DomainErrorDto currentError = null;

            while (currentException != null)
            {
                var error = ExceptionToError(exception, userMessage, showSystemErrors);

                if (resultError == null)
                {
                    resultError = error;
                }

                if (currentError != null)
                {
                    currentError.ErrorInfo.InnerError = error;
                }

                currentException = exception.InnerException;
                currentError = error.ErrorInfo?.InnerError;
            }

            return resultError;
        }

        private static DomainErrorDto ExceptionToError(Exception exception, string userMessage, bool showSystemErrors = false)
        {
            var error = new DomainErrorDto
            {
                UserMessage = userMessage
            };

            if (exception == null)
                return error;

            if (!showSystemErrors)
                return error;

            var errorInfo = new DomainErrorInfo
            {
                Message = exception.Message,
                StackTrace = exception.StackTrace
            };

            error.ErrorInfo = errorInfo;

            return error;
        }
    }
}