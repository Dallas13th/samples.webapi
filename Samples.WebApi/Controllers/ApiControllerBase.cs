﻿using Microsoft.AspNetCore.Mvc;
using Samples.WebApi.Models.Result;

namespace Samples.WebApi.Controllers
{
    //this RouteAttribute needs for swagger in route {controller}/{action} instead of {controller}/{id}
    [Route("api/[controller]/[action]")]
    public abstract class ApiControllerBase : ControllerBase
    {
        protected ApiResult<TContent> SuccessResult<TContent>(TContent content)
        {
            return new ApiResult<TContent>(content);
        }
    }
}