﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Samples.Domain.Models.Dto;
using Samples.Domain.Services;
using Samples.WebApi.Models.Result;

namespace Samples.WebApi.Controllers
{
    public class RowVersionEntityController : ApiControllerBase
    {
        private readonly RowVersionEntityService _service;

        public RowVersionEntityController(RowVersionEntityService service)
        {
            _service = service;
        }
        
        [HttpGet]
        [ProducesResponseType(typeof(ApiResult<List<RowVersionEntityDto>>), 200)]
        public async Task<ApiResult<RowVersionEntitiesResultDto>> GetEntitiesByVersionAsync(int rowVersionLong)
        {
            var result = await _service.GetAllEntitiesByRowVersionAsync(rowVersionLong);

            return SuccessResult(result);
        }
        
        [HttpPost]
        [ProducesResponseType(typeof(ApiResult<RowVersionEntityDto>), 200)]
        public async Task<ApiResult<RowVersionEntityDto>> CreateGenreAsync([FromQuery] DateTimeOffset dateTimeOffset)
        {
            var result = await _service.CreateRowVersionEntityAsync(dateTimeOffset);

            return SuccessResult(result);
        }
        
        [HttpPost]
        [ProducesResponseType(typeof(ApiResult<RowVersionEntityDto>), 200)]
        public async Task<ApiResult<RowVersionEntityDto>> UpdateGenreAsync([FromBody] RowVersionEntityDto dto)
        {
            var result = await _service.UpdateRowVersionEntityAsync(dto);

            return SuccessResult(result);
        }

        [HttpPost]
        [ProducesResponseType(typeof(ApiResult<bool>), 200)]
        public async Task<ApiResult<bool>> DeleteGenreAsync(int id)
        {
            await _service.DeleteRowVersionEntityAsync(id);

            return SuccessResult(true);
        }
    }
}
