﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Samples.Domain.Models.Dto;
using Samples.Domain.Models.Filters;
using Samples.Domain.Services;
using Samples.WebApi.Models.Result;

namespace Samples.WebApi.Controllers
{
    /// <summary>
    /// Authors CRUD
    /// </summary>
    public sealed class AuthorsController : ApiControllerBase
    {
        private readonly AuthorsService _authorsService;

        public AuthorsController(AuthorsService authorsService)
        {
            _authorsService = authorsService;
        }

        /// <summary>
        /// Get Authors
        /// </summary>
        /// <param name="filter"><inheritdoc cref="BaseFilter"/></param>
        /// <returns><inheritdoc cref="ApiResult{TContent}"/></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ApiResult<List<SimpleDto>>), 200)]
        public async Task<ApiResult<List<SimpleDto>>> GetAuthorsAsync(BaseFilter filter)
        {
            var result = await _authorsService.GetAuthorsAsync(filter);

            return SuccessResult(result);
        }

        /// <summary>
        /// Create author
        /// </summary>
        /// <param name="dto"><inheritdoc cref="SimpleDto"/></param>
        /// <returns><inheritdoc cref="ApiResult{TContent}"/></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResult<SimpleDto>), 200)]
        public async Task<ApiResult<SimpleDto>> CreateAuthorAsync([FromBody]SimpleDto dto)
        {
            var result = await _authorsService.CreateAuthorAsync(dto);

            return SuccessResult(result);
        }

        /// <summary>
        /// Update Author
        /// </summary>
        /// <param name="dto"><inheritdoc cref="SimpleDto"/></param>
        /// <returns><inheritdoc cref="ApiResult{TContent}"/></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResult<SimpleDto>), 200)]
        public async Task<ApiResult<SimpleDto>> UpdateAuthorAsync([FromBody]SimpleDto dto)
        {
            var result = await _authorsService.UpdateAuthorAsync(dto);

            return SuccessResult(result);
        }

        /// <summary>
        /// Delete author
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns><inheritdoc cref="ApiResult{TContent}"/></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResult<bool>), 200)]
        public async Task<ApiResult<bool>> DeleteAuthorAsync(int id)
        {
            var result = await _authorsService.DeleteAuthorAsync(id);

            return SuccessResult(result);
        }
    }
}