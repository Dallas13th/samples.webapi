﻿using Microsoft.AspNetCore.Mvc;

namespace Samples.WebApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public sealed class HomeController : ControllerBase
    {
        [HttpGet]
        public IActionResult Index()
        {
            const string relativePath = "~/swagger";

            var url = Url.Content(relativePath);

            return Redirect(url);
        }
    }
}