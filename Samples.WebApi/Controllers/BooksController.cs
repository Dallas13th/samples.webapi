﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Samples.Domain.Models.Dto;
using Samples.Domain.Models.Filters;
using Samples.Domain.Services;
using Samples.WebApi.Models.Result;

namespace Samples.WebApi.Controllers
{
    /// <summary>
    /// Books CRUD
    /// </summary>
    public sealed class BooksController : ApiControllerBase
    {
        private readonly BooksService _booksService;

        public BooksController(BooksService booksService)
        {
            _booksService = booksService;
        }

        /// <summary>
        /// Get Books
        /// </summary>
        /// <param name="filter"><inheritdoc cref="BooksFilter"/></param>
        /// <returns><inheritdoc cref="ApiResult{TContent}"/></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ApiResult<List<BookDto>>), 200)]
        public async Task<ApiResult<List<BookDto>>> GetBooksAsync(BooksFilter filter)
        {
            var result = await _booksService.GetBooksAsync(filter);

            return SuccessResult(result);
        }
        
        /// <summary>
        /// Create Book
        /// </summary>
        /// <param name="dto"><inheritdoc cref="BookDto"/></param>
        /// <returns><inheritdoc cref="ApiResult{TContent}"/></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResult<BookDto>), 200)]
        public async Task<ApiResult<BookDto>> CreateBookAsync([FromBody]BookDto dto)
        {
            var result = await _booksService.CreateBookAsync(dto);

            return SuccessResult(result);
        }

        /// <summary>
        /// Update Book
        /// </summary>
        /// <param name="dto"><inheritdoc cref="BookDto"/></param>
        /// <returns><inheritdoc cref="ApiResult{TContent}"/></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResult<BookDto>), 200)]
        public async Task<ApiResult<BookDto>> UpdateBookAsync([FromBody]BookDto dto)
        {
            var result = await _booksService.UpdateBookAsync(dto);

            return SuccessResult(result);
        }

        /// <summary>
        /// Delete book
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns><inheritdoc cref="ApiResult{TContent}"/></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResult<bool>), 200)]
        public async Task<ApiResult<bool>> DeleteBookAsync(int id)
        {
            var result = await _booksService.DeleteBookAsync(id);

            return SuccessResult(result);
        }
    }
}