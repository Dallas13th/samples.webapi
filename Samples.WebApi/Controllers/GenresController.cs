﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Samples.Domain.Models.Dto;
using Samples.Domain.Models.Filters;
using Samples.Domain.Services;
using Samples.WebApi.Models.Result;

namespace Samples.WebApi.Controllers
{
    /// <summary>
    /// Genres CRUD
    /// </summary>
    public sealed class GenresController : ApiControllerBase
    {
        private readonly GenresService _genresService;

        public GenresController(GenresService genresService)
        {
            _genresService = genresService;
        }

        /// <summary>
        /// Get Genres
        /// </summary>
        /// <param name="filter"><inheritdoc cref="BaseFilter"/></param>
        /// <returns><inheritdoc cref="ApiResult{TContent}"/></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ApiResult<List<SimpleDto>>), 200)]
        public async Task<ApiResult<List<SimpleDto>>> GetGenresAsync(BaseFilter filter)
        {
            var result  = await _genresService.GetGenresAsync(filter);

            return SuccessResult(result);
        }

        /// <summary>
        /// Create Genre
        /// </summary>
        /// <param name="dto"><inheritdoc cref="SimpleDto"/></param>
        /// <returns><inheritdoc cref="ApiResult{TContent}"/></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResult<SimpleDto>), 200)]
        public async Task<ApiResult<SimpleDto>> CreateGenreAsync([FromBody]SimpleDto dto)
        {
            var result = await _genresService.CreateGenreAsync(dto);

            return SuccessResult(result);
        }

        /// <summary>
        /// Update Genre
        /// </summary>
        /// <param name="dto"><inheritdoc cref="SimpleDto"/></param>
        /// <returns><inheritdoc cref="ApiResult{TContent}"/></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResult<SimpleDto>), 200)]
        public async Task<ApiResult<SimpleDto>> UpdateGenreAsync([FromBody]SimpleDto dto)
        {
            var result = await _genresService.UpdateGenreAsync(dto);

            return SuccessResult(result);
        }

        /// <summary>
        /// Delete Genre
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns><inheritdoc cref="ApiResult{TContent}"/></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ApiResult<bool>), 200)]
        public async Task<ApiResult<bool>> DeleteGenreAsync(int id)
        {
            var result = await _genresService.DeleteGenreAsync(id);

            return SuccessResult(result);
        }
    }
}