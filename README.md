# README #

# For project compilation make sure you have installed:
* Latest [Visual studio 2019](https://visualstudio.microsoft.com/downloads/)
* Latest [.Net Core and .Net Framework SDK](https://www.microsoft.com/net/download/visual-studio-sdks)

# Getting Started:
- Clone, build and deploy [database project](https://bitbucket.org/Dallas13th/samples.database)
- In Samples.WebApi and Samples.DataAccess.IntegrationTests projects copy and rename file
```
appsettings.template.json
```
to
```
appsettings.json
```

- Specify connection string or set "UseInMemoryContext" to "true" in appsettings file

# Tools, which may be useful for working with this project:
- [EF Core Power Tools](http://vsixgallery.com/extension/f4c4712c-ceae-4803-8e52-0e2049d5de9f/)
- [tangible T4 Editor](https://t4-editor.tangible-engineering.com/T4-Editor-Visual-T4-Editing.html)