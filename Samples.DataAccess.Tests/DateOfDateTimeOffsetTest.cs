﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Samples.DataAccess.Contexts;
using Xunit;

namespace Samples.DataAccess.Tests
{
    public sealed class DateOfDateTimeOffsetTest
    {
        [Fact]
        public async Task DateTimeOffset_ShouldFilterAndSelect_DateTime()
        {
            await using var context = new SamplesInMemoryContext();

            var minDate = await context.DataTypesTestTable.MaxAsync(d => d.DateTimeOffsetValue.DateTime);

            var notMinDate = await context.DataTypesTestTable
                .Where(w => w.DateTimeOffsetValue.DateTime != minDate)
                .Select(s => s.DateTimeOffsetValue.DateTime)
                .FirstOrDefaultAsync();

            Assert.NotEqual(minDate, notMinDate);
        }
    }
}