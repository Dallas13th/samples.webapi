﻿using EntityFrameworkExtensions.Attributes;

namespace Samples.DataAccess.Tests.CustomModels
{
    public sealed class CustomBookModel
    {
        [SortOption("Name")]
        public string BookName { get; set; }
    }
}