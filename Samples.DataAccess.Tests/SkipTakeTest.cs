﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Samples.DataAccess.Contexts;
using Xunit;

namespace Samples.DataAccess.Tests
{
    public sealed class SkipTakeTest
    {
        [Fact]
        public async Task SkipQuery_ShouldWork_WithoutOrderBy()
        {
            await using var context = new SamplesInMemoryContext();

            var skippedEntity = await context.DataTypesTestTable.FirstOrDefaultAsync();

            var skipResult = await context.DataTypesTestTable
                .Skip(1)
                .ToListAsync();

            bool isPassed = skipResult.All(w => w.Id != skippedEntity.Id);

            Assert.True(isPassed);
        }
    }
}