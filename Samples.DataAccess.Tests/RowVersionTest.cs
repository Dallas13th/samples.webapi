﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using EntityFrameworkExtensions;
using Microsoft.EntityFrameworkCore;
using Samples.DataAccess.Contexts;
using Xunit;

namespace Samples.DataAccess.Tests
{
    public sealed class RowVersionTest
    {
        [Fact]
        public async Task RowVersion_Should_BeComparableInLinqToSqlQuery()
        {
            await using var context = new SamplesInMemoryContext();

            var firstEntity = await context.DataTypesTestTable.FirstOrDefaultAsync();

            Assert.NotNull(firstEntity);

            var query = context.DataTypesTestTable
                .Where(w => w.Id != firstEntity.Id
                            && StructuralComparisons.StructuralComparer
                                .Compare(w.RowVersion, firstEntity.RowVersion) > 0);
            
            bool notEqualsFirstExists = await query.AnyAsync();

            Assert.True(notEqualsFirstExists);
        }

        [Theory]
        [InlineData(13L)]
        [InlineData(1_234_567_891_234_567_89L)]
        public void RowVersion_ToInt64_AndBack_ShouldReturn_TheSameValue(long expected)
        {
            var bytes = expected.Int64ToRowVersionBytes();

            var result = bytes.RowVersionToInt64();

            Assert.Equal(expected, result);
        }

        [Fact]
        public void RowVersion_ToReversedBytes_ShouldReturn_ReversedBytes()
        {
            const long sourceValue = 13L;

            var result = sourceValue.Int64ToRowVersionBytes();

            var allBytesExceptLastAreZero = result.Take(7).All(w => w == 0);

            Assert.True(allBytesExceptLastAreZero);
            Assert.Equal(13, result[7]);
        }

        [Fact]
        public void RowVersion_FromReversedBytes_ShouldReturn_ExpectedValue()
        {
            var expected = 13L;
            var source = new byte[] { 0, 0, 0, 0, 0, 0, 0, 13 };
            
            var result = source.RowVersionToInt64();

            Assert.Equal(expected, result);
        }
    }
}