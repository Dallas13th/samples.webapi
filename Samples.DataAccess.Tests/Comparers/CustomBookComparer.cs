﻿using System.Collections.Generic;
using Samples.DataAccess.Tests.CustomModels;

namespace Samples.DataAccess.Tests.Comparers
{
    public sealed class CustomBookComparer : IEqualityComparer<CustomBookModel>
    {
        public bool Equals(CustomBookModel x, CustomBookModel y)
        {
            if (x == null && y == null) return true;
            if (x == null || y == null) return false;

            if (x == y) return true;

            return x.BookName == y.BookName;
        }

        public int GetHashCode(CustomBookModel obj)
        {
            return obj.GetHashCode();
        }
    }
}