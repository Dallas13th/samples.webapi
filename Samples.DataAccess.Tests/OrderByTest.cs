﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntityFrameworkExtensions;
using EntityFrameworkExtensions.Models;
using Microsoft.EntityFrameworkCore;
using Samples.DataAccess.Contexts;
using Samples.DataAccess.GeneratedContext;
using Samples.DataAccess.Tests.Comparers;
using Samples.DataAccess.Tests.CustomModels;
using Xunit;

namespace Samples.DataAccess.Tests
{
    public sealed class OrderByTest
    {
        [Fact]
        public async Task OrderedByNameBooks_ShouldHave_SameSequence()
        {
            using (var context = new SamplesInMemoryContext())
            {
                var expected = Enumerable.Range(0, 10)
                    .Select(s => "BookName_" + s)
                    .OrderByDescending(o => o);

                var sortOptions = new List<SortOption>
                {
                    new SortOption
                    {
                        ColumnName = nameof(Book.Name),
                        IsDescending = true
                    }
                };

                var query = context.Book.OrderBy(sortOptions);

                var result = await query.Select(s => s.Name).ToListAsync();

                Assert.True(expected.SequenceEqual(result));
            }
        }

        [Fact]
        public async Task OrderedByCustomBookProperties_ShouldHave_SameSequence()
        {
            using (var context = new SamplesInMemoryContext())
            {
                var sortOptions = new List<SortOption>
                    {
                        new SortOption
                        {
                            ColumnName = nameof(CustomBookModel.BookName),
                            IsDescending = true
                        }
                    }
                    .GetAttributeCorrectedProperties<CustomBookModel>();

                var expected = Enumerable.Range(0, 10)
                    .Select(s => new CustomBookModel
                    {
                        BookName = "BookName_" + s
                    })
                    .OrderByDescending(o => o.BookName);

                var query = context.Book
                    .OrderBy(sortOptions)
                    .Select(s => new CustomBookModel
                    {
                        BookName = s.Name
                    });

                var result = await query.ToListAsync();

                Assert.True(expected.SequenceEqual(result, new CustomBookComparer()));
            }
        }
    }
}